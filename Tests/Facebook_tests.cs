﻿namespace Tests
{
    using System.Data.Entity;
    using System.Linq;
    using Facebook;
    using Namaz.Domain;
    using Namaz.Model.Dto;
    using Newtonsoft.Json;
    using Xunit;

    public class FacebookTests
    {
        [Fact]
        void Test_Me()
        {
            var appSecret = "7aca263d09b71ee3123562855b1f7be4";
            var accessToken =
                "CAAE2JKnUxJIBANZAw5iuiYn65mSs7beK5u1vl8wEPns599D4ZBmzJxIPN0Cjhv0JoLq9L5EAUKztAKFgf4Bw7FeeJvDvcgDf3wgaZAxnQv1WveDaFN8AvpQknrPYRwtrr8QmVg0LP5DwzYXoZAwQ3KwJkiNZCKp9TDFkcHq3xp00hBu8EfPTMMPBN2ZBa6bq5cbw97zMOoaBZBjoOc0mO3kZCE9MD1RZAKNPMHZBSOZB2Ev2QZDZD";

            var cl = new FacebookClient(accessToken);
            var me = cl.Get("me");
        }

        [Fact]
        void Populate_Email()
        {
            using (var ctx = new NamazDbContext())
            {
                var providers = ctx.AuthProviders.Include(provider => provider.User).ToList();

                foreach (var prov in providers)
                {
                    try
                    {
                        var cl = new FacebookClient(prov.CurrentToken);
                        dynamic me = cl.Get("me");

                        var email = me.email;

                        prov.User.Email = email;

                        ctx.Users.Attach(prov.User);
                    }
                    catch { }
                }

                ctx.SaveChanges();
            }
        }

        [Fact]
        void SerializeUser()
        {
            var user = new UserDto
            {
                Token =
                    "CAAE2JKnUxJIBANZAw5iuiYn65mSs7beK5u1vl8wEPns599D4ZBmzJxIPN0Cjhv0JoLq9L5EAUKztAKFgf4Bw7FeeJvDvcgDf3wgaZAxnQv1WveDaFN8AvpQknrPYRwtrr8QmVg0LP5DwzYXoZAwQ3KwJkiNZCKp9TDFkcHq3xp00hBu8EfPTMMPBN2ZBa6bq5cbw97zMOoaBZBjoOc0mO3kZCE9MD1RZAKNPMHZBSOZB2Ev2QZDZD"
            };

            var text = JsonConvert.SerializeObject(user);
        }
    }
}
