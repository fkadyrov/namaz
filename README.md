Запросы посылать через GET

# Работа с расписанием
# **Получение расписания по идентификатору:** #
## schedule/get##
Параметры:

```
#!javascript

id = 1
```
Ответ:

```
#!javascript
    
{
    success: true,
    data: {
      "Id": "1",
      "AuthorName": "AuthorName",
      "Month": "1",
      "CityId": "1",
      "Days": [
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": {
               "Name": "1",
               "Time": "123123"
            },
            "Magrib": null,
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": null,
            "Magrib": null,
            "Isha": {
               "Name": "1",
               "Time": "123123"
            }
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": null,
            "Magrib": {
               "Name": "1",
               "Time": "123123"
            },
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": {
               "Name": "1",
               "Time": "123123"
            },
            "Asr": null,
            "Magrib": null,
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": {
               "Name": "1",
               "Time": "123123"
            },
            "Zuhr": null,
            "Asr": null,
            "Magrib": null,
            "Isha": null
         }
      ]
   }
}
```


# **Добавление расписания юзеру:** #
## schedule/apply ##
Параметры:

```
#!javascript

userDto = { "Email": "test@test.com", "Password": "123" }
scheduleId = 1
```

# **Создание расписания:** #
## schedule/create ##
Параметры:

```
#!javascript

userDto = { "Email": "test@test.com", "Password": "123" }
scheduleDto = {
      "Month: 1,
      "CityId": "1",
      "Days": [
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": {
               "Name": "1",
               "Time": "123123"
            },
            "Magrib": null,
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": null,
            "Magrib": null,
            "Isha": {
               "Name": "1",
               "Time": "123123"
            }
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": null,
            "Magrib": {
               "Name": "1",
               "Time": "123123"
            },
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": {
               "Name": "1",
               "Time": "123123"
            },
            "Asr": null,
            "Magrib": null,
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": {
               "Name": "1",
               "Time": "123123"
            },
            "Zuhr": null,
            "Asr": null,
            "Magrib": null,
            "Isha": null
         }
      ]
   }
```
Ответ:

```
#!javascript

{success: true, data: 1}
```

# **Редактирование расписания** #
## schedule/edit
Параметры:

```
#!javascript

userDto = { "Email": "test@test.com", "Password": "123" }
scheduleDto = {
      "Month: 1,
      "CityId": "1",
      "Days": [
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": {
               "Name": "1",
               "Time": "123123"
            },
            "Magrib": null,
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": null,
            "Magrib": null,
            "Isha": {
               "Name": "1",
               "Time": "123123"
            }
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": null,
            "Asr": null,
            "Magrib": {
               "Name": "1",
               "Time": "123123"
            },
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": null,
            "Zuhr": {
               "Name": "1",
               "Time": "123123"
            },
            "Asr": null,
            "Magrib": null,
            "Isha": null
         },
         {
            "DayOfMonth": 0,
            "Fadzhr": null,
            "Voshod": {
               "Name": "1",
               "Time": "123123"
            },
            "Zuhr": null,
            "Asr": null,
            "Magrib": null,
            "Isha": null
         }
      ]
   }
```
Ответ:

```
#!javascript

{success: true, data: 1}
```

# Создание расписания, даже если такое уже существует
## **schedule/createifexists**

# Увеличть рейтинг раасписания
## **schedule/increaserate**

# Уменьшить рейтинг раписания
## **schedule/decreaserate**

# Получить расписание по юзеру
## **schedule/getschedulebyuser**

# Получить расписания по городу 
## **schedule/getschedulesbycity**

# Получить следующее раписание 
## **schedule/getnext**

# Работа с пользователями
# **Создание пользователя:** #
## user/create
Параметры:

```
#!javascript

userDto = {
   Name: "",
   Password: "",
   Email: ""
}
```
Ответ:

```
#!javascript

{success: true, data: 1}
```

# **Изменение параметров пользователя:** #
## user/changesmth
Параметры:

```
#!javascript

  userDto = {
     Password: "",
     Email: ""
  }
  field = ""
  newVal = ""

```

# Проверить существует ли пользователь 
## **user/exists**

# Сбросить пароль 
## **user/resetpassword**

# Осуществить рассылку 
## **user/sendmessages*

# Получить статистику по расписаниям
## stats/counters