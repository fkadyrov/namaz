﻿using System;

namespace Namaz.Domain
{
    public class NamazException : Exception
    {
        public string Description { get; set; }

        public NamazException(string code, string description = null) : base(code)
        {
            Description = description;
        }
    }
}