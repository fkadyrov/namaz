﻿namespace Namaz.Domain
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Monads;
    using AutoMapper;
    using Model;
    using Model.Dto;

    public class ScheduleRepo
    {
        private UserRepo _userRepo;
        private int _tokenExp = 10;

        public ScheduleRepo()
        {
            _userRepo = new UserRepo();
        }

        public ResultDto Create(UserDto userDto, ScheduleDto schedule)
        {
            if (userDto == null || schedule == null)
                throw new NamazException(ErrorCodes.ScheduleErrors.ParametersAreEmpty);

            object result = null;
            bool success = true;
            // Считаем хеш
            var hash = new NamazHasher().CreateHash(schedule);

            using (var db = new NamazDbContext())
            {
                // Находим юзера
                var user = _userRepo.Get(userDto);
                var city = db.Cities.FirstOrDefault(x => x.ExtId == schedule.CityId);

                if (city == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.CityNotFound);
                }

                // Юзер пуст - ничего не делаем
                if (user == null)
                {
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);
                }

                var cityId = 0L;
                long.TryParse(schedule.CityId, out cityId);

                var inMonthSchedule = db.Schedules
                    .Include(x => x.Creator)
                    .Include(x => x.City)
                    .SingleOrDefault(
                        x => 
                        x.Creator.Id == user.Id 
                        && x.City.Id == city.Id 
                        && x.Month == schedule.Month
                        && x.Year == schedule.Year);
                if (inMonthSchedule.IsNotNull())
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.ExistsForUserCityAndMonth);
                }

                // Ищем расписание по хешу
                var existedOrNewSchedule = db.Schedules.Include(x => x.Creator).FirstOrDefault(x => x.Hash == hash);

                // Если нет существующего - создаем
                if (existedOrNewSchedule == null)
                {
                    existedOrNewSchedule = Mapper.Map<Schedule>(schedule, opts => opts.CreateMissingTypeMaps = true);
                    existedOrNewSchedule.Creator = user;
                    existedOrNewSchedule.Hash = hash;
                    existedOrNewSchedule.City = city;

                    db.Users.Attach(user);
                    db.Schedules.Add(existedOrNewSchedule);

                    var previousMonthSchedule =
                        db.Schedules.SingleOrDefault(
                            x =>
                                x.Creator.Id == user.Id
                                && x.City.Id == city.Id
                                &&
                                (schedule.Month > 1 && x.Month == (schedule.Month - 1) && x.Year == schedule.Year ||
                                 schedule.Month == 1 && x.Month == 12 && x.Year == (schedule.Year - 1)));
                    
                    // Если для юзера в этом городе в прошлом месяце было расписание
                    // Связываем старое и новое
                    if (previousMonthSchedule != null)
                    {
                        previousMonthSchedule.Next = existedOrNewSchedule;
                        //existedSchedule.Counter += previousMonthSchedule.Counter;
                        //db.Entry(previousMonthSchedule).State = EntityState.Modified;
                        db.Schedules.Attach(previousMonthSchedule);
                    }

                    // Сохраняем все
                    db.SaveChanges();

                    result = existedOrNewSchedule.Id;
                }
                else
                {
                    result = new {authorName = existedOrNewSchedule.Creator.Name, scheduleId = existedOrNewSchedule.Id};
                    success = false;
                }
            }

            return new ResultDto {Success = success, Message = "Saved", Data = result};
        }

        public ResultDto Edit(UserDto userDto, ScheduleDto schedule, long scheduleId)
        {
            if (userDto == null || schedule == null || scheduleId <= 0)
                throw new NamazException(ErrorCodes.ScheduleErrors.ParametersAreEmpty);

            object result = null;

            // Считаем хеш
            var hash = new NamazHasher().CreateHash(schedule);

            using (var db = new NamazDbContext())
            {
                var user = _userRepo.Get(userDto);

                // Юзер пуст - ничего не делаем
                if (user == null)
                {
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);
                }

                var city = db.Cities.FirstOrDefault(x => x.ExtId == schedule.CityId);

                if (city == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.CityNotFound);
                }

                var existedSchedule = db.Schedules.Single(x => x.Id == scheduleId);
                Mapper.Map(schedule, existedSchedule, typeof(ScheduleDto), typeof(Schedule),
                    opts => opts.CreateMissingTypeMaps = true);
                existedSchedule.Id = scheduleId;
                existedSchedule.Creator = user;
                existedSchedule.Hash = hash;
                existedSchedule.City = city;

                db.Users.Attach(user);

                db.SaveChanges();
                result = existedSchedule.Id;
            }

            return new ResultDto { Success = true, Message = "Saved", Data = result };
        }

        public ResultDto CreateIfExists(UserDto userDto, ScheduleDto schedule)
        {
            if (userDto == null || schedule == null)
                throw new NamazException(ErrorCodes.ScheduleErrors.ParametersAreEmpty);

            object result = null;

            // Считаем хеш
            var hash = new NamazHasher().CreateHash(schedule);

            using (var db = new NamazDbContext())
            {
                // Находим юзера
                var user = _userRepo.Get(userDto);

                var city = db.Cities.FirstOrDefault(x => x.ExtId == schedule.CityId);

                if (city == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.CityNotFound);
                }

                // Юзер пуст - ничего не делаем
                if (user == null)
                {
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);
                }

                var cityId = 0L;
                long.TryParse(schedule.CityId, out cityId);

                var newSchedule = Mapper.Map<Schedule>(schedule, opts => opts.CreateMissingTypeMaps = true);
                newSchedule.Creator = user;
                newSchedule.Hash = hash;
                newSchedule.City = city;
                db.Schedules.Add(newSchedule);

                db.Users.Attach(user);
                // Сохраняем все
                db.SaveChanges();

                result = newSchedule.Id;
            }

            return new ResultDto { Success = true, Message = "Saved", Data = result };
        }

        public ResultDto Apply(UserDto userDto, int scheduleId)
        {
            if (userDto.IsNull())
                throw new NamazException(ErrorCodes.ScheduleErrors.ParametersAreEmpty);

            // Считаем хеш
            using (var db = new NamazDbContext())
            {
                // Находим юзера
                var user = _userRepo.Get(userDto);

                // Юзер пуст - ничего не делаем
                if (user == null)
                {
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);
                }

                // Ищем расписание по идентификатору
                var schedule = db.Schedules.FirstOrDefault(x => x.Id == scheduleId);

                // Если нет существующего - ошибку выдаем
                if (schedule == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.NotFoundById);
                }

                if (user.Schedule != null)
                {
                    user.Schedule.Counter--;
                    db.Schedules.Attach(user.Schedule);
                }

                // Указываем расписание для юзера
                user.Schedule = schedule;

                schedule.Counter++;
                db.Schedules.Attach(schedule);

                // Сохраняем все
                db.SaveChanges();
            }

            return new ResultDto { Success = true, Message = "Saved" };
        }

        public ResultDto Get(int id)
        {
            using (var db = new NamazDbContext())
            {
                var schedule = db.Schedules
                    .Include(x => x.Creator)
                    .Include(x => x.City)
                    .FirstOrDefault(x => x.Id == id);

                object result = null;

                if (schedule != null)
                {
                    var dto = Mapper.Map<ScheduleDto>(schedule, options => options.CreateMissingTypeMaps = true);
                    dto.AuthorName = schedule.Creator.With(x => x.Name);
                    dto.CityId = schedule.City.With(x => x.ExtId);

                    result = dto;
                }

                return new ResultDto { Success = true, Data = result };
            }
        }

        public ResultDto IncreaseRate(long scheduleId)
        {
            using (var ctx = new NamazDbContext())
            {
                var schedule = ctx.Schedules.Find(scheduleId);

                if (schedule == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.NotFoundById);
                }

                schedule.Counter++;

                ctx.SaveChanges();
            }

            return ResultDto.Succeded();
        }

        public ResultDto DecreaseRate(long scheduleId)
        {
            using (var ctx = new NamazDbContext())
            {
                var schedule = ctx.Schedules.Find(scheduleId);

                if (schedule == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.NotFoundById);
                }

                schedule.Counter--;

                ctx.SaveChanges();
            }

            return ResultDto.Succeded();
        }

        public ResultDto GetSchedulesByUser(UserDto userDto)
        {
            using (var db = new NamazDbContext())
            {
                // Находим юзера
                var user = _userRepo.Get(userDto);

                if (user == null)
                {
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);
                }

                var schedules = db.Schedules
                    .Include(x => x.Creator)
                    .Include(x => x.City)
                    .Where(x => x.Creator.Id == user.Id)
                    .Select(x => new
                    {
                        id = x.Id,
                        source = x.Source,
                        cityId = x.City.ExtId,
                        countryId = x.City.CountryExtId,
                        regionId = x.City.RegionExtId,
                        year = x.Year,
                        month = x.Month,
                        counter = x.Counter
                    }).ToList();

                return new ResultDto() { Success = true, Data = schedules };
            }
        }

        public ResultDto GetSchedulesByCity(string cityId)
        {
            using (var db = new NamazDbContext())
            {
                var schedules = db.Schedules
                    .Include(x => x.Creator)
                    .Where(x => x.City.ExtId == cityId)
                    .Select(x => new
                    {
                        id = x.Id,
                        authorName = x.Creator.Name,
                        source = x.Source,
                        counter = x.Counter,
                        month = x.Month,
                        year = x.Year
                    }).ToList();

                return new ResultDto() { Success = true, Data = schedules };
            }
        }

        public ResultDto GetNext(long scheduleId)
        {
            using (var db = new NamazDbContext())
            {
                var next = db.Schedules.Include(x => x.Next).FirstOrDefault(x => x.Id == scheduleId).With(x => x.Next);

                if (next == null)
                {
                    throw new NamazException(ErrorCodes.ScheduleErrors.NoNextSchedule);
                }

                return ResultDto.Succeded(new {nextId = next.Id});
            }
        }
    }
}