﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Namaz.Domain
{
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Hosting;
    using Model;
    using Newtonsoft.Json;

    public class CityRepo
    {
        public static string Populate()
        {
            var path = HostingEnvironment.MapPath("~/cities/cities.json");
            if (!File.Exists(path))
            {
                return string.Empty;
            }

            var holders =
                JsonConvert.DeserializeObject<CitiesHolder[]>(File.ReadAllText(path, Encoding.UTF8));

            var counter = 0;

            var cityList = new List<City>();

            var sb = new StringBuilder();

            if (holders == null || !holders.Any())
            {
                return string.Empty;
            }

            using (var ctx = new NamazDbContext())
            {
                var existingCities = GetExistingCityIds(ctx);
                var holderDict = ctx.CitiesHolders.ToDictionary(x => x.ExtId);

                List<CitiesHolder> nonExisting = FilterExisting(holders, existingCities);

                foreach (var holder in nonExisting)
                {
                    counter++;

                    CitiesHolder dbHolder;
                    if (!holderDict.TryGetValue(holder.ExtId, out dbHolder))
                    {
                        dbHolder = holder;
                        ctx.CitiesHolders.Add(dbHolder);
                    }
                    
                    if (holder.Cities == null && holder.Regions == null)
                        continue;

                    if (holder.Cities == null)
                    {
                        holder.Cities = new Collection<City>();
                    }

                    //PrepareHolder(holder);

                    foreach (var city in holder.Cities)
                    {
                        city.Holder = dbHolder;
                        cityList.Add(city);
                    }
                }

                ctx.SaveChanges();
            }

            //  var i = 0;
            //  foreach (var city in cityList)
            //    {

            //        sb.AppendLine(string.Format("INSERT INTO city(id, name_en, ext_id, country_ext_id, lat, lng, region_ext_id, holder_id, \"Region_Id\") VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, '{8}');",
            //            ++i, city.NameEn, city.ExtId, city.CountryExtId, city.Lat, city.Lng, city.RegionExtId, city.Holder.Id, city.RegionExtId));
            //    }
            //var res = sb.ToString();
            //return res;

            return null;
        }

        private static void PrepareHolder(CitiesHolder holder)
        {
            if (holder.Regions != null && holder.Regions.Count > 0)
            {
               foreach (var region in holder.Regions)
                {
                    foreach (var city in region.Cities)
                    {
                        city.RegionExtId = region.Id;
                        holder.Cities.Add(city);   
                    }
                }
            }
        }

        private static List<string> GetExistingCityIds(NamazDbContext ctx)
        {
            return ctx.Cities.Select(x => x.ExtId).ToList();
        }

        private static List<string> GetExistingHolders(NamazDbContext ctx)
        {
            return ctx.CitiesHolders.Select(x => x.ExtId).ToList();
        }

        private static List<CitiesHolder> FilterExisting(CitiesHolder[] holders, List<string> existingCities)
        {
            var cSet = new HashSet<string>(existingCities);
            var result = new List<CitiesHolder>();

            foreach (var holder in holders)
            {
                var cities = new List<City>();

                if (holder.Cities != null)
                {
                    foreach (var hCity in holder.Cities)
                    {
                        if (!cSet.Contains(hCity.ExtId))
                        {
                            cities.Add(hCity);
                        }
                    }
                }

                if (holder.Regions != null)
                {
                    foreach (var region in holder.Regions)
                    {
                        if (region.Cities != null)
                        {
                            foreach (var rCity in region.Cities)
                            {
                                if (!cSet.Contains(rCity.ExtId))
                                {
                                    rCity.RegionExtId = region.Id;
                                    cities.Add(rCity);
                                }
                            }
                        }
                    }
                }

                if (cities.Any())
                {
                    holder.Cities = cities;
                    result.Add(holder);
                }
            }

            return result;
        }
    }
}