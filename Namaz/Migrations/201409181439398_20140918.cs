namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20140918 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.schedule", "year", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.schedule", "year");
        }
    }
}
