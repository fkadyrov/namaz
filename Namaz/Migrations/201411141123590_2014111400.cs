namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2014111400 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.auth_provider", "date_create", c => c.DateTime(nullable: true));
            AddColumn("public.auth_provider", "date_edit", c => c.DateTime(nullable: true));
            AddColumn("public.user", "date_create", c => c.DateTime(nullable: true));
            AddColumn("public.user", "date_edit", c => c.DateTime(nullable: true));
            AddColumn("public.schedule", "date_create", c => c.DateTime(nullable: true));
            AddColumn("public.schedule", "date_edit", c => c.DateTime(nullable: true));
            AddColumn("public.city", "date_create", c => c.DateTime(nullable: true));
            AddColumn("public.city", "date_edit", c => c.DateTime(nullable: true));
            AddColumn("public.cities_holder", "date_create", c => c.DateTime(nullable: true));
            AddColumn("public.cities_holder", "date_edit", c => c.DateTime(nullable: true));

            DefaultifyDateTime("public.auth_provider");
            DefaultifyDateTime("public.user");
            DefaultifyDateTime("public.schedule");
            DefaultifyDateTime("public.city");
            DefaultifyDateTime("public.cities_holder");
        }
        
        public override void Down()
        {
            DropColumn("public.cities_holder", "date_edit");
            DropColumn("public.cities_holder", "date_create");
            DropColumn("public.city", "date_edit");
            DropColumn("public.city", "date_create");
            DropColumn("public.schedule", "date_edit");
            DropColumn("public.schedule", "date_create");
            DropColumn("public.user", "date_edit");
            DropColumn("public.user", "date_create");
            DropColumn("public.auth_provider", "date_edit");
            DropColumn("public.auth_provider", "date_create");
        }

        private void DefaultifyDateTime(string table)
        {
            Sql(string.Format("update {0} set date_create = to_date('01.01.0001', 'DD.MM.YYYY') where date_create is null", table));
            Sql(string.Format("update {0} set date_edit = to_date('01.01.0001', 'DD.MM.YYYY') where date_edit is null", table));
        }
    }
}
