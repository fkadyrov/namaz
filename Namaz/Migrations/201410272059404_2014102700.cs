namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2014102700 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.auth_provider", "ext_id", c => c.String(maxLength: 1073741823, fixedLength: true));
        }
        
        public override void Down()
        {
            DropColumn("public.auth_provider", "ext_id");
        }
    }
}
