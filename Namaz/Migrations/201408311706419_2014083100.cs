namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2014083100 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.schedule", "city_id", c => c.Long(nullable: false));
            CreateIndex("public.schedule", "city_id");
            AddForeignKey("public.schedule", "city_id", "public.city", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("public.schedule", "city_id", "public.city");
            DropIndex("public.schedule", new[] { "city_id" });
            DropColumn("public.schedule", "city_id");
        }
    }
}
