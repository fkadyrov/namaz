namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.user", "schedule_id", "public.schedule");
            DropIndex("public.user", new[] { "schedule_id" });
            AddColumn("public.schedule", "source", c => c.String(maxLength: 1073741823, fixedLength: true));
            AlterColumn("public.user", "schedule_id", c => c.Long());
            CreateIndex("public.user", "schedule_id");
            AddForeignKey("public.user", "schedule_id", "public.schedule", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("public.user", "schedule_id", "public.schedule");
            DropIndex("public.user", new[] { "schedule_id" });
            AlterColumn("public.user", "schedule_id", c => c.Long(nullable: false));
            DropColumn("public.schedule", "source");
            CreateIndex("public.user", "schedule_id");
            AddForeignKey("public.user", "schedule_id", "public.schedule", "id", cascadeDelete: true);
        }
    }
}
