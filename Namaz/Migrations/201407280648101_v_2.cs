namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.user", "salt", c => c.String(maxLength: 1073741823, fixedLength: true));
        }
        
        public override void Down()
        {
            DropColumn("public.user", "salt");
        }
    }
}
