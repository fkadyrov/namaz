﻿namespace Namaz.ModelBinder
{
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Newtonsoft.Json;

    public class JsonBinder<T> : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var key =
                controllerContext.HttpContext.Request.Params.AllKeys.FirstOrDefault(
                    x => x.Trim().ToLower() == typeof (T).Name.ToLower());

            if (string.IsNullOrEmpty(key))
                return default(T);

            string text = controllerContext.HttpContext.Request[key];

            text = HttpUtility.UrlDecode(text, Encoding.UTF8);

            if (string.IsNullOrEmpty(text))
                return default(T);

            var result = JsonConvert.DeserializeObject(text, typeof(T));

            return result;
        }
    }
}