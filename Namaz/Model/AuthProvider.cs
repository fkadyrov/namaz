﻿namespace Namaz.Model
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("auth_provider")]
    public class AuthProvider : BaseEntity
    {
        public User User { get; set; }

        [Column("code")]
        public string ProviderCode { get; set; }

        [Column("cur_token")]
        public string CurrentToken { get; set; }

        [Column("old_token")]
        public string OldToken { get; set; }

        [Column("last_check")]
        public DateTime LastCheckDate { get; set; }

        [Column("ext_id")]
        public string ExtId { get; set; }
    }
}