﻿namespace Namaz.Model.Dto
{
    using System.Monads;

    public class UserDto
    {
        private string _name;
        private string _email;

        public string Name
        {
            get { return _name; }
            set { _name = value.With(x => x.Trim()); }
        }

        public string Password { get; set; }

        public string Email
        {
            get { return _email; }
            set { _email = value.With(x => x.Trim()); }
        }

        public string Token { get; set; }
    }
}