﻿namespace Namaz.Model
{
    using System.Collections.Generic;

    public interface ISchedule
    {
        int Month { get; set; }

        List<PrayDay> Days { get; set; }
    }
}