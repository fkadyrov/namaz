﻿namespace Namaz.Model
{
    public interface IHaveId
    {
        long Id { get; set; }
    }
}