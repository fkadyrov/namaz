﻿namespace Namaz.Controllers
{
    using System.Web.Mvc;
    using Domain;
    using Model.Dto;
    using ModelBinder;

    public class ScheduleController : BaseController
    {
        [HttpGet]
        public ActionResult Create(
            [ModelBinder(typeof(JsonBinder<UserDto>))] UserDto userDto,
            [ModelBinder(typeof(JsonBinder<ScheduleDto>))] ScheduleDto scheduleDto)
        {
            return InTryCatch(() => new ScheduleRepo().Create(userDto, scheduleDto));
        }

        [HttpGet]
        public ActionResult Edit(
            [ModelBinder(typeof(JsonBinder<UserDto>))] UserDto userDto,
            [ModelBinder(typeof(JsonBinder<ScheduleDto>))] ScheduleDto scheduleDto,
            long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().Edit(userDto, scheduleDto, scheduleId));
        }

        [HttpGet]
        public ActionResult CreateIfExists(
            [ModelBinder(typeof(JsonBinder<UserDto>))] UserDto userDto,
            [ModelBinder(typeof(JsonBinder<ScheduleDto>))] ScheduleDto scheduleDto)
        {
            return InTryCatch(() => new ScheduleRepo().CreateIfExists(userDto, scheduleDto));
        }

        [HttpGet]
        public ActionResult Apply(
            [ModelBinder(typeof(JsonBinder<UserDto>))] UserDto userDto,
            int scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().Apply(userDto, scheduleId));
        }

        [HttpGet]
        public ActionResult Get(int id)
        {
            return InTryCatch(() => new ScheduleRepo().Get(id));
        }

        public ActionResult IncreaseRate(long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().IncreaseRate(scheduleId));
        }

        public ActionResult DecreaseRate(long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().DecreaseRate(scheduleId));
        }

        [HttpGet]
        public ActionResult GetSchedulesByUser([ModelBinder(typeof (JsonBinder<UserDto>))] UserDto userDto)
        {
            return InTryCatch(() => new ScheduleRepo().GetSchedulesByUser(userDto));
        }

        public ActionResult GetSchedulesByCity(string cityId)
        {
            return InTryCatch(() => new ScheduleRepo().GetSchedulesByCity(cityId));
        }

        public ActionResult GetNext(long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().GetNext(scheduleId));
        }
    }
}