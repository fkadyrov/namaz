﻿namespace Namaz.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Domain;

    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return null;
        }

        public ActionResult PopulateCities()
        {
            CityRepo.Populate();

            return null;
        }
    }
}