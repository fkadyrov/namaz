﻿namespace Namaz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Domain;
    using Model;
    using Model.Dto;
    using ModelBinder;

    public class UserController : BaseController
    {
        private UserRepo _userRepo;

        public UserController()
        {
            _userRepo = new UserRepo();
        }

        [HttpGet]
        public ActionResult Exists(string email, string password)
        {
            return InTryCatch(() => _userRepo.CheckUser(email, password));
        }

        [HttpGet]
        public ActionResult Create(
            [ModelBinder(typeof(JsonBinder<UserDto>))]UserDto userDto)
        {
            return InTryCatch(() => _userRepo.Create(userDto));
        }

        [HttpGet]
        public ActionResult ChangeSmth(
            [ModelBinder(typeof(JsonBinder<UserDto>))]UserDto userDto, 
            string field, 
            string newVal)
        {
            return InTryCatch(() => _userRepo.ChangeSmth(userDto, field, newVal));
        }

        [HttpGet]
        public ActionResult ResetPassword(string email, string locale)
        {
            return InTryCatch(() => _userRepo.ResetPassword(email, locale));
        }

        public ActionResult SendMessages(string language, string month1, string month2, bool? test)
        {
            return InTryCatch(() =>
            {
                using (var ctx = new NamazDbContext())
                {
                    var sender = new MailSender(ctx);

                    var users = FetchUsersWithPreviousOrCurrentSchedules(ctx);

                    if (test.GetValueOrDefault())
                    {
                        users = new List<User>()
                        {
                            new User()
                            {
                                Name = "Tagir",
                                Email = "tagir.nafikov@gmail.com"
                            }
                        };
                    }
                    else
                    {
                        users.Add(new User()
                        {
                            Name = "Tagir",
                            Email = "tagir.nafikov@gmail.com"
                        });
                    }

                    SendMails(language, month1, month2, users, sender);

                    return ResultDto.Succeded();
                }
            });
        }

        public ActionResult SendTextMessage(string message, bool? test)
        {
            return InTryCatch(() =>
            {
                using (var ctx = new NamazDbContext())
                {
                    var users = FetchUsersWithPreviousOrCurrentSchedules(ctx);

                    if (test.GetValueOrDefault())
                    {
                        users = new List<User>()
                        {
                            new User()
                            {
                                Name = "Tagir",
                                Email = "tagir.nafikov@gmail.com"
                            }
                        };
                    }
                    else
                    {
                        users.Add(new User()
                        {
                            Name = "Tagir",
                            Email = "tagir.nafikov@gmail.com"
                        });
                    }

                    var sender = new MailSender(ctx);

                    Parallel.ForEach(users,
                        new ParallelOptions()
                        {
                            MaxDegreeOfParallelism = 4
                        },
                        user => { sender.SendMessage(user, message); });
                }

                return ResultDto.Succeded();
            });
        }

        private List<User> FetchUsersWithPreviousOrCurrentSchedules(NamazDbContext ctx)
        {
            var now = DateTime.UtcNow;
            var month = now.Month;
            var year = now.Year;

            var users = ctx.Users
                .Where(x => x.Email != null)
                .Where(x => ctx.Schedules.Any(
                    s =>
                        s.Creator.Id == x.Id
                        && ((month > 1 && s.Month == (month - 1) && s.Year == year ||
                             month == 1 && s.Month == 12 && s.Year == (year - 1)) ||
                            s.Month == month && s.Year == year)))
                .ToList();
            return users;
        }

        private void SendMails(string language, string month1, string month2, List<User> users, MailSender sender)
        {
            Parallel.ForEach(users,
                new ParallelOptions()
                {
                    MaxDegreeOfParallelism = 4
                },
                user => { sender.SendMessage(user, language, month1, month2); });
        }
    }
}