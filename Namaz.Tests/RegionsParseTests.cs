﻿namespace Namaz.Tests
{
    using System.Text;
    using Model;
    using Newtonsoft.Json;
    using Xunit;

    public class RegionsParseTests
    {
        [Fact]
        public void Deserialize()
        {
            var obj =
                JsonConvert.DeserializeObject<CitiesHolder[]>(Encoding.UTF8.GetString(Properties.Resources.All_cities));

            Assert.NotNull(obj);
        } 
    }
}