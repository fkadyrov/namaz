﻿namespace Namaz.Tests
{
    using System.Collections.Generic;
    using Domain;
    using Model;
    using Xunit;
    using Xunit.Should;

    public class ScheduleComparisonTests
    {
        [Fact]
        public void Two_Equal_Schedultes_Should_Provide_Same_Hash()
        {
            var sch1 = new Schedule
            {
                Month = 1,
                Days = new List<PrayDay>
                {
                    new PrayDay
                    {
                        DayOfMonth = 1,
                        Asr = new PrayTime {Name = "Asr", Time = "00:11"},
                        Fadzhr = new PrayTime {Name = "Fadzhr", Time = "13:11"}
                    }
                }
            };

            var sch2 = new Schedule
            {
                Month = 1,
                Days = new List<PrayDay>
                {
                    new PrayDay
                    {
                        DayOfMonth = 1,
                        Asr = new PrayTime {Name = "Asr", Time = "00:11"},
                        Fadzhr = new PrayTime {Name = "Fadzhr", Time = "13:11"}
                    }
                }
            };

            var hasher = new NamazHasher();

            hasher.CreateHash(sch1).ShouldBe(hasher.CreateHash(sch2));
        }

        [Fact]
        public void Two_Different_Schedules_Provide_Different_Hash()
        {
            var sch1 = new Schedule
            {
                Month = 1,
                Days = new List<PrayDay>
                {
                    new PrayDay
                    {
                        DayOfMonth = 1,
                        Asr = new PrayTime {Name = "Asr", Time = "00:11"},
                        Fadzhr = new PrayTime {Name = "Fadzhr", Time = "13:11"}
                    }
                }
            };

            var sch2 = new Schedule
            {
                Month = 1,
                Days = new List<PrayDay>
                {
                    new PrayDay
                    {
                        DayOfMonth = 1,
                        Asr = new PrayTime {Name = "Asr", Time = "00:12"},
                        Fadzhr = new PrayTime {Name = "Fadzhr", Time = "13:11"}
                    }
                }
            };

            var hasher = new NamazHasher();
            var hash1 = hasher.CreateHash(sch1);
            var hash2 = hasher.CreateHash(sch2);

            hash1.ShouldNotBe(hash2);
        }
    }
}
