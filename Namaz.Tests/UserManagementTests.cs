﻿namespace Namaz.Tests
{
    using System.Linq;
    using System.Net;
    using Domain;
    using Model.Dto;
    using Xunit;

    public class UserManagementTests
    {
        private UserRepo _repo;

        ~UserManagementTests()
        {
            using (var db = new NamazDbContext())
            {
                db.Users.RemoveRange(db.Users);
            }
        }

        public UserManagementTests()
        {
            _repo = new UserRepo();
        }

        [Fact]
        public void Create_User()
        {
            var dto = new UserDto
            {
                Email = "test@test.com",
                Name = "Phil",
                Password = "123"
            };

            var result = _repo.Create(dto);

            Assert.True(result.Success);

            using (var db = new NamazDbContext())
            {
                var user = db.Users.SingleOrDefault(x => x.Email == dto.Email);

                Assert.NotNull(user);
                Assert.Equal(user.Name, dto.Name);

                db.Users.Remove(user);
                db.SaveChanges();
            }
        }

        [Fact]
        public void Change_Email()
        {
            var dto = new UserDto
            {
                Email = "test@test.com",
                Name = "Phil",
                Password = "123"
            };

            var result = _repo.Create(dto);

            long id = 0;
            using (var db = new NamazDbContext())
            {
                var user = _repo.GetByEmail(dto.Email);
                id = user.Id;
            }

            result = _repo.ChangeSmth(dto, "Email", "ololo@mail.com");

            Assert.True(result.Success);

            using (var db = new NamazDbContext())
            {
                var user = db.Users.Find(id);

                db.Users.Remove(user);

                db.SaveChanges();

                Assert.Equal("ololo@mail.com", user.Email);
            }
        }

        [Fact]
        public void Change_With_Duplicate_Email_Fails()
        {
            var dto = new UserDto
            {
                Email = "test@test.com",
                Name = "Phil",
                Password = "123"
            };

            _repo.Create(dto);
            dto.Email = "ololo@mail.com";
            _repo.Create(dto);

            var result = _repo.ChangeSmth(dto, "Email", "test@test.com");

            Assert.False(result.Success);
            Assert.Equal("Уже есть пользователь с таким email", result.Message);

            using (var db = new NamazDbContext())
            {
                db.Users.RemoveRange(db.Users);
                db.SaveChanges();
            }
        }

        [Fact]
        public void Create_User_From_Json()
        {
            var data = "{\"userInfo\":{  \"Name\":\"tagir_test\",\"Password\":\"12345\",\"Email\":\"superhaker06@mail.ru\"}}";

            using (var cl = new WebClient())
            {
                cl.UploadString("", data);
            }
        }
    }
}